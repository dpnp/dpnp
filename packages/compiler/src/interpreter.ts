/**
 * @dpnp/compiler
 * src/interpreter.ts
 */


import { ParsedNode, ParsedType } from '@dpnp/parser'


export function interpret(rootNode: ParsedNode.Schema) {
  let flatDeclarations: Map<string[], ParsedNode.CoreDeclaration> = new Map()

  let registerDeclaration = (node: ParsedNode.Declaration, path: string[] = []) => {
    let declPath = [...path, node.name]

    if (node.type !== 'NamespaceDeclaration') {
      for (let [otherPath, otherDecl] of flatDeclarations) {
        if (arrEquals(otherPath, declPath)) {
          throw new Error(`InterpretedNode '${path.join('::')}' already exists`)
        }
      }

      flatDeclarations.set(declPath, node)
    }

    if (node.type === 'InterfaceDeclaration' || node.type === 'NamespaceDeclaration') {
      for (let declNode of node.declarations) {
        registerDeclaration(declNode, declPath)
      }
    }
  }

  for (let declNode of rootNode.declarations) {
    registerDeclaration(declNode)
  }


  let nodes: InterpretedNode.Node[] = []

  let transformType = (node: ParsedType.Type, path: string[], generics: string[] = []): InterpretedType.Type => {
    switch (node.type) {
      case 'ArrayType':
        return {
          kind: InterpretedType.Kind.Array,
          type: transformType(node.value, path, generics)
        }

      case 'PrimitiveType':
        switch (node.name) {
          case 'bool': return { kind: InterpretedType.Kind.Bool }
          case 'byte': return { kind: InterpretedType.Kind.Byte }

          case 'fstring': return { kind: InterpretedType.Kind.FiniteString }
          case 'string': return { kind: InterpretedType.Kind.String }

          case 'float': return { kind: InterpretedType.Kind.Float }
          case 'double': return { kind: InterpretedType.Kind.Double }

          case 'int': return { kind: InterpretedType.Kind.Int, signed: false, minSize: 1 }
          case 'int32': return { kind: InterpretedType.Kind.Int, signed: false, minSize: 4 }
          case 'int64': return { kind: InterpretedType.Kind.Int, signed: false, minSize: 8 }

          case 'fixed32': return { kind: InterpretedType.Kind.FixedInt, signed: false, size: 4 }
          case 'fixed64': return { kind: InterpretedType.Kind.FixedInt, signed: false, size: 8 }

          default: throw new Error(`Unknown primitive type '${node.name}'`)
        }

      case 'ReferenceType':
        let contextPath = Array.from(path)

        while (true) {
          let fullPath = [...contextPath, ...node.target]
          let index = 0

          for (let [otherPath, declaration] of flatDeclarations) {
            if (arrEquals(otherPath, fullPath)) {

              if (declaration.type !== 'EnumDeclaration') {
                if (declaration.generics.length !== node.generics.length) {
                  throw new Error(`Unexpected ${node.generics.length} generic arguments for ${fullPath.join('::')}<${declaration.generics.join(', ')}>`)
                }
              } else if (node.generics.length > 0) {
                throw new Error(`Unexpected generic arguments for enum ${fullPath.join('::')}`)
              }


              return {
                kind: InterpretedType.Kind.Reference,
                generics: node.generics.map((type) => transformType(type, path, generics)),
                target: index
              }
            }

            index++
          }

          if (contextPath.length < 1) break
          contextPath.pop()
        }

        if (node.target.length === 1) {
          let genericIndex = generics.indexOf(node.target[0])

          if (genericIndex >= 0) {
            return {
              kind: InterpretedType.Kind.GenericReference,
              target: genericIndex
            }
          }
        }

        throw new Error(`Missing object '${node.target.join('::')}' from '${path.join('::')}'`)
    }
  }

  for (let [fullPath, declaration] of flatDeclarations) {
    let path = fullPath.slice(0, -1)
    let name = fullPath.slice(-1)[0]

    let flags: InterpretedNode.Flags = {
      compact: declaration.flags.has('compact'),
      entry: declaration.flags.has('entry'),
      obsolete: declaration.flags.has('obsolete')
    }

    switch (declaration.type) {
      case 'AliasDeclaration':
        nodes.push({
          kind: InterpretedNode.Kind.Alias,
          name, path,
          ...flags,

          generics: declaration.generics,
          value: transformType(declaration.value, fullPath, declaration.generics)
        })

        break

      case 'InterfaceDeclaration':
        let generics = declaration.generics

        nodes.push({
          kind: InterpretedNode.Kind.Interface,
          name, path,
          ...flags,

          fields: declaration.fields.map(([name, type]) =>
            [name, transformType(type, fullPath, generics)]
          ),
          generics
        })

        break

      case 'EnumDeclaration':
        nodes.push({
          kind: InterpretedNode.Kind.Enum,
          name, path,
          ...flags,

          items: declaration.items,
          type: {
            kind: InterpretedType.Kind.FixedInt,
            signed: false,
            size: Math.ceil(Math.log(declaration.items.length) / Math.log(2) / 8)
          }
        })

        break

      default: throw new Error('what?')
    }
  }


  return nodes
}

function arrEquals<T>(arr1: T[], arr2: T[]): boolean {
  if (arr1.length !== arr2.length) return false

  for (let index = 0; index < arr1.length; index++) {
    if (arr1[index] !== arr2[index]) return false
  }

  return true
}


export namespace InterpretedType {
  export enum Kind {
    Array, Bool, Byte, Double, FiniteString, FixedInt, Float, GenericReference, Int, /* Object, */ Reference, String
  }

  interface Bool { kind: Kind.Bool }
  interface Byte { kind: Kind.Byte }

  interface FiniteString { kind: Kind.FiniteString }
  interface String { kind: Kind.String }

  interface Float { kind: Kind.Float }
  interface Double { kind: Kind.Double }

  interface Int {
    kind: Kind.Int
    signed: boolean
    minSize: number
  }

  interface FixedInt {
    kind: Kind.FixedInt
    signed: boolean
    size: number
  }

  /* interface Object {
    kind: Kind.Object
    fields: Type[]
  } */

  interface Array {
    kind: Kind.Array
    type: Type
  }

  interface Reference {
    kind: Kind.Reference
    generics: InterpretedType.Type[]
    target: number
  }

  interface GenericReference {
    kind: Kind.GenericReference
    target: number
  }

  export type Type = Array | Bool | Byte | Double | FiniteString | FixedInt | Float | GenericReference | Int | /* Object | */ Reference | String
}

export namespace InterpretedNode {
  export enum Kind { Alias, Interface, Enum }

  export interface Flags {
    compact: boolean
    entry: boolean
    obsolete: boolean
  }

  export interface Base extends Flags {
    kind: Kind
    name: string
    path: string[]
  }

  export interface Alias extends Base {
    kind: Kind.Alias

    generics: string[]
    value: InterpretedType.Type
  }

  export interface Interface extends Base {
    kind: Kind.Interface

    fields: [string | null, InterpretedType.Type][]
    generics: string[]
  }

  export interface Enum extends Base {
    kind: Kind.Enum

    items: string[]
    type: InterpretedType.Type
  }

  export type Node = Alias | Interface | Enum
}

