/**
 * @dpnp/compiler
 * src/generators/ecmascript.ts
 */


import { InterpretedNode, InterpretedType } from '../interpreter'


export function generateECMAScript(nodes: InterpretedNode.Node[]): string {
  let output = '/* This file was generated automatically. */\n\nvar lib = require("@dpnp/library");\n\n'

  let generateForType = (type: InterpretedType.Type): [string, string] => {
    switch (type.kind) {
      case InterpretedType.Kind.Array:
        let [encodeCode, decodeCode] = generateForType(type.type)

        return [
          `lib.encodeArray.bind(void 0, ${encodeCode})`,
          `lib.decodeArray.bind(void 0, ${decodeCode})`
        ]

      case InterpretedType.Kind.Bool:
        return ['lib.encodeBool', 'lib.decodeBool']

      case InterpretedType.Kind.Byte:
        return ['((value, write) => write(value))', '((read) => read())']

      case InterpretedType.Kind.Double:
        return ['lib.encodeDouble', 'lib.decodeDouble']

      case InterpretedType.Kind.FixedInt: {
        let suffix = `.bind(void 0, { signed: ${type.signed ? 'true' : 'false' }, size: ${type.size} })`

        return [
          'lib.encodeFixedInt' + suffix,
          'lib.decodeFixedInt' + suffix
        ]
      }

      case InterpretedType.Kind.Int: {
        let suffix = `.bind(void 0, { signed: ${type.signed ? 'true' : 'false' }, minSize: ${type.minSize} })`

        return [
          'lib.encodeComposite' + suffix,
          'lib.decodeComposite' + suffix
        ]
      }

      case InterpretedType.Kind.Reference:
        let target = nodes[type.target]

        if (target.compact) {
          let [encodeCode, decodeCode] = generateForType((target as InterpretedNode.Alias).value)

          if (type.generics.length > 0) {
            console.warn(`Reference to [compact] node '${target.name}' with generic arguments is not supported`)
          }

          return [
            `(() => { return ${encodeCode}; })()`,
            `(() => { return ${decodeCode}; })()`
          ]
        }

        let targetIdentifier = target.path.map((s) => s + '.').join('')
        let genericArgumentTypes = type.generics.map((arg) => generateForType(arg))

        return [
          `${targetIdentifier}_encode${target.name}.bind(void 0${genericArgumentTypes.map((arg) => ', ' + arg[0]).join('')})`,
          `${targetIdentifier}_decode${target.name}.bind(void 0${genericArgumentTypes.map((arg) => ', ' + arg[1]).join('')})`
        ]

      case InterpretedType.Kind.GenericReference:
        return [
          `_encodeGeneric${type.target}`,
          `_decodeGeneric${type.target}`
        ]

      case InterpretedType.Kind.FiniteString:
        return ['lib.encodeFiniteString', 'lib.decodeFiniteString']

      case InterpretedType.Kind.String:
        return ['lib.encodeString', 'lib.decodeString']
    }

    return [
      '(() => { throw new Error("Not implemented"); })',
      '(() => { throw new Error("Not implemented"); })'
    ]
  }

  for (let node of nodes) {
    if (node.compact) {
      continue
    }

    let name = node.name
    let fullName = [...node.path, name].join('::')

    let level = 0
    let closingCode = ''

    let segments = ['exports', ...node.path]
    let parentNamespace = segments.slice(-1)[0]

    for (let [parent, child] of groupByTwo(segments)) {
      output += `${spaces(level)}var ${child};\n`
      output += `${spaces(level)}(function (${child}) {\n`

      closingCode = `${spaces(level)}})(${child} = ${parent}.${child} || (${parent}.${child} = {}));\n` + closingCode

      level++
    }


    let additionalCode: string[] = []
    let encodeFunctionCode: string[] = []
    let decodeFunctionCode: string[] = []

    if (node.obsolete) {
      encodeFunctionCode.push(`console.warn("${fullName} is obsolete");`)
      decodeFunctionCode.push(`console.warn("${fullName} is obsolete");`)
    }

    switch (node.kind) {
      case InterpretedNode.Kind.Alias: {
        let [encodeCode, decodeCode] = generateForType(node.value)

        encodeFunctionCode.push(`${encodeCode}(value, write);`)
        decodeFunctionCode.push(`return ${decodeCode}(read);`)

        break
      }

      case InterpretedNode.Kind.Interface: {
        decodeFunctionCode.push('let output = {};')

        for (let [fieldName, fieldType] of node.fields) {
          let [encodeCode, decodeCode] = generateForType(fieldType)

          encodeFunctionCode.push(`${encodeCode}(${fieldName !== null ? `value.${fieldName}` : 'null' }, write);`)
          decodeFunctionCode.push((fieldName !== null ? `output.${fieldName} = ` : '') + `${decodeCode}(read);`)
        }

        decodeFunctionCode.push('return output;')

        break
      }

      case InterpretedNode.Kind.Enum: {
        let [encodeCode, decodeCode] = generateForType(node.type)

        encodeFunctionCode.push(`${encodeCode}(value, write);`)
        decodeFunctionCode.push(`return ${decodeCode}(read);`)

        additionalCode.push(`var ${name} = {};`)

        for (let index = 0; index < node.items.length; index++) {
          let item = node.items[index]
          additionalCode.push(`${name}[${name}["${item}"] = ${index}] = "${item}";`)
        }

        additionalCode.push('')
        additionalCode.push(`${parentNamespace}.${name} = ${name};\n`)

        break
      }
    }


    let encodeFunctionName = `encode${name}`
    let decodeFunctionName = `decode${name}`

    if (node.entry) {
      additionalCode.push(`${parentNamespace}.${encodeFunctionName} = lib.encodeRootFromBytes.bind(void 0, _${encodeFunctionName});`)
      additionalCode.push(`${parentNamespace}.${decodeFunctionName} = lib.decodeRootFromBytes.bind(void 0, _${decodeFunctionName});`)
      additionalCode.push('')
    }


    let genericArgumentsSizeArray = node.kind !== InterpretedNode.Kind.Enum
      ? new Array(node.generics.length).fill(0)
      : []

    let encodeGenericArguments = genericArgumentsSizeArray.map((_, index) => `_encodeGeneric${index}, `).join('')
    let decodeGenericArguments = genericArgumentsSizeArray.map((_, index) => `_decodeGeneric${index}, `).join('')

    output += [
      ...additionalCode,
      `function _${encodeFunctionName}(${encodeGenericArguments}value, write) {`,
      ...encodeFunctionCode.map(indentLine(1)),
      '}\n',

      `function _${decodeFunctionName}(${decodeGenericArguments}read) {`,
      ...decodeFunctionCode.map(indentLine(1)),
      '}\n',

      `${parentNamespace}._${encodeFunctionName} = _${encodeFunctionName};`,
      `${parentNamespace}._${decodeFunctionName} = _${decodeFunctionName};\n`
    ].map(indentLine(node.path.length)).join('\n')

    output += closingCode
    output += '\n'
  }

  return output
}


function groupByTwo<T>(arr: T[]) {
  let output: [T | null, T][] = []

  for (let index = 0; index < arr.length; index++) {
    if (index !== arr.length - 1) output.push([arr[index], arr[index + 1]])
  }

  return output
}

function indentLine(level: number) {
  return (line: string) => {
    return spaces(level) + line
  }
}

function spaces(level: number): string {
  return '  '.repeat(level)
}

