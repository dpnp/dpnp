/**
 * @dpnp/compiler
 * src/generators/index.ts
 */


import { generateECMAScript } from './ecmascript'


export const Generators = {
  js: generateECMAScript,
}

