/**
 * @dpnp/cli
 * src/cli.ts
 */


import * as fs from 'fs'
import * as minimist from 'minimist'
import * as path from 'path'

import { Generators, interpret } from '@dpnp/compiler'
import { parse } from '@dpnp/parser'


export function cli(argv = process.argv.slice(2)) {
  let args = minimist<CommandArguments>(argv)

  if (args.h || args.help) {
    console.log('dpnp CLI 0.1.0')
    process.exit(0)

    return
  }

  let input = fs.readFileSync(computeAbsolutePath(args._[0])).toString()

  // console.log(inspect(parse(input), { colors: true, depth: null }))
  // return

  let interpreted = interpret(parse(input))

  for (let [outputFormat, outputPath] of args._.slice(1).map((a) => a.split(':'))) {
    if (!(outputFormat in Generators)) {
      throw new Error(`Unknown output format '${outputFormat}'`)
    }

    let result = Generators[outputFormat as keyof typeof Generators](interpreted)
    fs.writeFileSync(computeAbsolutePath(outputPath), result)
  }
}


function computeAbsolutePath(inputPath: string): string {
  return path.isAbsolute(inputPath)
    ? inputPath
    : path.join(process.cwd(), inputPath)
}


interface CommandArguments {
  h: boolean
  help: boolean
}

