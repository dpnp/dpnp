{
  function arrSplit(arr, differentiator) {
    let a = []
    let b = []

    arr.forEach((item, index) => {
      if (differentiator(item, index, arr)) {
        a.push(item)
      } else {
        b.push(item)
      }
    })

    return [a, b]
  }
}


Schema
  = _ options:(o:OptionSequence __ { return o })? declarations:(d:Declaration __ { return d })* {
    return {
      type: 'Schema',
      declarations,
      options
    }
  }

AliasDeclaration "alias declaration"
  = "type" __ name:DeclarationNameGenerics flags:DeclarationFlags? _ "=" _ value:Type {
    return {
      ...name,
      type: 'AliasDeclaration',
      flags: new Set(flags),
      value
    }
  }

EnumDeclaration "enum declaration"
  = "enum" __ name:CamelCase flags:DeclarationFlags? _ "{" _ items:CamelCaseList "}" {
    return {
      type: 'EnumDeclaration',
      name,
      items,
      flags: new Set(flags)
    }
  }

ExternalDeclaration "external declaration"
  = "external" __ nameFirst:CamelCase nameOthers:("::" o:CamelCase { return o })*  {
    let fullPath = [nameFirst, ...nameOthers]

    return {
      type: 'ExternalDeclaration',
      name: fullPath.slice(-1)[0],
      path: fullPath.slice(0, -1)
    }
  }

InterfaceDeclaration "interface declaration"
  = "interface" __ name:DeclarationNameGenerics flags:DeclarationFlags? _ block:InterfaceBlock {
    let [fields, declarations] = arrSplit(block, (expr) => expr.type === 'FieldDefinition')

    return {
      ...name,
      type: 'InterfaceDeclaration',
      flags: new Set(flags),

      fields: fields.map((f) => [f.name, f.value]),
      declarations
    }
  }

NamespaceDeclaration "namespace declaration"
  = "namespace" __ name:CamelCase _ "{" _ declarations:(d:Declaration __ { return d })* "}" {
    return {
      type: 'NamespaceDeclaration',
      name,
      declarations
    }
  }

DeclarationNameGenerics
  = name:CamelCase generics:("<" _ list:CamelCaseList ">" { return list })? {
    return {
      name,
      generics: generics || []
    }
  }

DeclarationFlags "declaration flags"
  = __ "[" _
    flags:(first:LowerCase others:(_ "," _ item:LowerCase { return item })* { return [first, ...others] })?
  _ "]" __ {
    return new Set(flags)
  }

Declaration
  = declaration:(AliasDeclaration / EnumDeclaration / ExternalDeclaration / InterfaceDeclaration / NamespaceDeclaration) {
    return {
      ...declaration,
      location: location()
    }
  }


InterfaceBlock
  = "{" _ exprs:((FieldDefinition / Declaration) __)* "}" {
    return exprs.map((e) => e[0])
  }

FieldDefinition
  = name:LowerCase ":" __ value:Type {
    return {
      type: 'FieldDefinition',
      name,
      value
    }
  }
  / "{@}:" __ value:Type {
    return {
      type: 'FieldDefinition',
      name: null,
      value
    }
  }

Type
  = type:(ArrayType / PrimitiveType / RepeatedType / ReferenceType) {
    return {
      ...type,
      location: location()
    }
  }

ArrayType
  = "array<" value:Type ">" {
    return {
      type: 'ArrayType',
      value
    }
  }

RepeatedType
  = "repeated<" value:Type ">(" times:Integer ")" {
    return {
      type: 'RepeatedType',
      value,
      times
    }
  }

PrimitiveType "primitive type"
  = [a-z]+ [0-9]* { return { type: 'PrimitiveType', name: text() } }

ReferenceType "reference type"
  // = value:CamelCase { return { type: 'ReferenceType', value } }
  = first:CamelCase others:("::" CamelCase)* generics:("<" _ TypeList ">")? {
    return {
      type: 'ReferenceType',
      target: [first, ...others.map((o) => o[1])],
      generics: generics ? generics[2] : []
    }
  }

TypeList
  = head:Type _ tail:("," _ item:Type _ { return item })* { return [head, ...tail] }
  / "" { return [] }


/* Option sequence */

OptionSequence "option sequence"
  = "[options]" _ "{" _ items:(i:OptionItem __ { return i })* _ "}" { return items }

OptionItem
  = name:SnakeCase _ "=" _ value:OptionValue {
    return {
      name, value,
      location: location()
    }
  }

OptionValue
  = "true" { return { type: 'OptionValueBool', value: true } }
  / "false" { return { type: 'OptionValueBool', value: false } }
  / value:HexadecimalByteList { return { type: 'OptionValueBytes', value } }
  / value:Integer { return { type: 'OptionValueInteger', value } }

HexadecimalByteList
  = first:HexadecimalByte others:(_ "," _ b:HexadecimalByte { return b })* {
    return [first, ...others]
  }


/* Utilities */

Integer
  = [0-9]+ { return parseInt(text(), 10) }

HexadecimalByte
  = "0x" HexadecimalChar HexadecimalChar { return parseInt(text()) }

HexadecimalChar
  = [0-9] / [a-f] { return text() }

CamelCase
  = [A-Z] ([A-Z] / [a-z])* {
    return text()
  }

CamelCaseList
  = head:CamelCase _ tail:("," _ item:CamelCase _ { return item })* { return [head, ...tail] }
  / "" { return [] }

LowerCase
  = [a-z]+ {
    return text()
  }

SnakeCase
  = [a-z] ([a-z] / "_")* {
    return text()
  }

_ "whitespace"
  = [ \t\n\r]*

__ "mandatory whitespace"
  = [ \t\n\r]+

