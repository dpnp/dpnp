/**
 * dpnp
 * src/parser.d.ts
 */


export function parse(input: string): ParsedNode.Schema


export namespace ParsedNode {
  export interface Base {
    type: string
  }

  export interface Schema extends Base {
    type: 'Schema'
    declarations: Declaration[]
  }

  export interface AliasDeclaration extends Base {
    type: 'AliasDeclaration'
    name: string

    flags: Set<string>
    generics: string[]

    value: ParsedType.Type
  }

  export interface EnumDeclaration extends Base {
    type: 'EnumDeclaration'
    name: string

    flags: Set<string>

    items: string[]
  }

  export interface InterfaceDeclaration extends Base {
    type: 'InterfaceDeclaration'
    name: string

    flags: Set<string>
    generics: string[]

    declarations: Declaration[]
    fields: [string | null, ParsedType.Type][]
  }

  export interface NamespaceDeclaration extends Base {
    type: 'NamespaceDeclaration'
    name: string

    declarations: Declaration[]
  }

  export type CoreDeclaration = AliasDeclaration | EnumDeclaration | InterfaceDeclaration
  export type Declaration = AliasDeclaration | EnumDeclaration | InterfaceDeclaration | NamespaceDeclaration
}


export namespace ParsedType {
  export interface ArrayType {
    type: 'ArrayType'
    value: Type
  }

  export interface PrimitiveType {
    type: 'PrimitiveType'
    name: string
  }

  export interface ReferenceType {
    type: 'ReferenceType'
    generics: Type[]
    target: string[]
  }

  export type Type = ArrayType | PrimitiveType | ReferenceType
}

